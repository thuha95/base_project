package com.hantt;

import com.hantt.service.MyInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {
    @Autowired
    private MyInterface callBack;
//    private MyInterface myInterface = new MyService();


    @GetMapping("/sum")
    Integer sum ( @RequestParam("a") Integer one, @RequestParam("b") Integer two){
        return one + two;
    }

    @GetMapping("/factorial")
    Integer factorial ( @RequestParam("a") Integer one){
        return callBack.doFactorial(one);
    }

}
