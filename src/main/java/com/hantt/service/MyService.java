package com.hantt.service;

import org.springframework.stereotype.Service;

@Service
public class MyService implements MyInterface {

    @Override
    public int doFactorial(int n) {
        int rs = 1;
        for (int i = 2; i <= n ; i++){
            rs = rs *i;
        }
        return rs;
    }
}
